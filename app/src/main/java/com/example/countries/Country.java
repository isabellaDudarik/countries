package com.example.countries;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Country {

    public Country(int id, String name, long population) {
        this.id = id;
        this.name = name;
        this.population = population;
    }

    public Country() {
    }

    @PrimaryKey
    public int id;
    public String name;
    public long population;
}
