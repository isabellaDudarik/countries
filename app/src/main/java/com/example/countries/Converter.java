package com.example.countries;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static List<Country> convert( List<RequestModel> requestModels) {
        List<Country> countries = new ArrayList<>();
        for (int i = 0; i < requestModels.size(); i++) {
            countries.add(new Country(i + 1, requestModels.get(i).name, requestModels.get(i).population));
        }
        return countries;
    }
}
