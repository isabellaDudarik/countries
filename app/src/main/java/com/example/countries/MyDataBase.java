package com.example.countries;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Country.class}, version = 6)
public abstract class MyDataBase extends RoomDatabase {
    public abstract CountryDao getCountryDao();
}
