package com.example.countries;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    TextView text;
    MyDataBase dataBase;
    Disposable disposable;
    CompositeDisposable disposables = new CompositeDisposable();

    EditText request;
    Button action;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.textView);
        request = findViewById(R.id.editText);
        action = findViewById(R.id.button);


        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printCountries();
            }
        });


        dataBase = Room.databaseBuilder(this, MyDataBase.class, "database")
                .fallbackToDestructiveMigration()
                .build();
        getinfo();
//        printCountries();
    }


    private void printCountries() {

        long numberFromRequest = Long.parseLong(request.getText().toString());
        long percentOfNumber = numberFromRequest * 15 / 100;
        long valueMin = numberFromRequest - percentOfNumber;
        long valueMax = numberFromRequest + percentOfNumber;

        disposables.add(
                dataBase.getCountryDao().selectAllByPopulation(valueMin, valueMax)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<List<Country>>() {
                            @Override
                            public void accept(List<Country> countries) throws Exception {
                                for (Country country : countries) {
//                                    long req = Long.parseLong(request.getText().toString());

//                                    if (req == country.population) {
                                        text.setText(String.format("%s has population: %s", country.name, country.population));
//                                    }

//                                    Log.d("data", country.name);
                                }
                            }
                        }));
    }

    @Override
    protected void onStop() {
        super.onStop();
        disposables.clear();
    }


    public void getinfo() {
        disposable = ApiService.getData()
                .map(new Function<List<RequestModel>, List<Country>>() {
                    @Override
                    public List<Country> apply(List<RequestModel> requestModels) throws Exception {
                        List<Country> countries = Converter.convert(requestModels);
                        dataBase.getCountryDao().updateAll(countries);
                        return countries;
                    }
                })


                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Country>>() {
                    @Override
                    public void accept(List<Country> list) throws Exception {
                        /* DO NOTHING */
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}

