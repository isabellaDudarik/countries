package com.example.countries;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class CountryDao {
    @Insert
    public abstract void insertAll(List<Country> countries);

//    @Query("SELECT * FROM Country")
//    public abstract List<Country> selectAll();


    @Query("SELECT * FROM Country WHERE population > :valueMin AND population < :valueMax")
    public abstract Flowable<List<Country>> selectAllByPopulation(long valueMin, long valueMax);


//    @Query("SELECT * FROM Country WHERE population = :request")
//    public abstract Flowable<List<Country>> selectAllByPopulation(long request);



    @Query("DELETE FROM Country")
    public abstract void removeAll();

    public  void updateAll(List<Country> countries){
        removeAll();
        insertAll(countries);
    }


}
