package com.example.countries;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public class ApiService {
    private static final String API = "https://restcountries.eu/rest/v2/";
    private static PrivateApi privateApi;

    public interface PrivateApi {
        @GET("all")
        Observable<List<RequestModel>> getCountries();

        @GET("population/{population}")
        Observable<List<RequestModel>> getPopulation(@Path("population") long population);
    }

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);
    }

    public static Observable<List<RequestModel>> getData() {
        return privateApi.getCountries();
    }

    public static Observable<List<RequestModel>> getPopulation(long population) {
        return privateApi.getPopulation(population);
    }


}
